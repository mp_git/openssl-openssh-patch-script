# OpenSSL, OpenSSH rebuild script 
# Author: JaeMoo Han 
#!/bin/sh

source ./bashlib_log.sh

current_root_dir=`pwd`

ssl_path="files/openssl"
ssh_path="files/openssh"
openssl_dir="${current_root_dir}/$ssl_path"
openssh_dir="${current_root_dir}/$ssh_path"
openssl_version="1.0.2r"
openssh_version="7.9p1"
openssl_file="openssl-${openssl_version}"
openssh_file="openssh-${openssh_version}"
openssl_prefix="/usr/local/openssl"
openssh_prefix="/usr/local/openssh"
openssl_build_conf_option="--openssldir=${openssl_prefix} shared threads zlib enable-ssl2"
openssh_build_conf_option="--prefix=${openssh_prefix} --with-ssl-dir=${openssl_prefix} --with-md5-passwords --without-zlib-version-check"

logs_dir="${current_root_dir}/logs"
work_dir="${current_root_dir}/workspace"
back_dir="${current_root_dir}/backup"
dateString=`date +"%Y%m%d_%H%M"`

Usage()
{
	echo "============================================================"
	echo "| OpenSSL, OpenSSH rebuild script"
	echo "| script usage"
	echo "|   ./rebuild.sh"
	echo "============================================================"
}

ParseArgument()
{
	args="$@"
	arg_num=$#
	if (( $# != 0 )) ;then
		echo "Not wanted argument!!!"
		echo ""
		Usage
		exit -1
	fi

	for arg in $args ;do
		case "$arg" in
			*)
				echo "Not wanted argument!!!"
				echo ""
				Usage
				exit -1
				;;
		esac
	done
	return
}

openssl_backup()
{
	LogWriter " * Backup /usr/local/openssl to $back_dir/openssl_bak$dateString"
	cp -rf /usr/local/openssl $back_dir/openssl_bak$dateString
	return
}

openssl_build()
{
	LogWriter " * Build OpenSSL. prefix is ${openssl_prefix}"
	if [ -f ${openssl_dir}/${openssl_file}".tar.gz" ] ;then
		LogWriter "   > Uncompress $openssl_dir/$openssl_file.tar.gz to $work_dir"
		tar -zxf ${openssl_dir}/${openssl_file}".tar.gz" -C $work_dir
	else
		LogWriter "   > ${openssl_dir}/${openssl_file}.tar.gz not exist"
		exit -1
	fi

	cd $work_dir/${openssl_file}

	LogWriter "   > config ${openssl_build_conf_option}"
	./config ${openssl_build_conf_option}

	LogWriter "   > make"
	make -j4
	LogWriter "   > remove /usr/local/openssl"
	rm -rf /usr/local/openssl

	LogWriter "   > make install"
	make install

	cd ${current_root_dir}
	return
}

openssl_binary_compress()
{
	LogWriter " * compress OpenSSL dir(/usr/local/openssl) to ./openssl_file/openssl.tar.gz"
	cd /usr/local
	if [ -d openssl ] ;then
		tar -zcf ${openssl_dir}"/"openssl.tar.gz openssl
	else
		LogWriter "   > [error] /usr/local/openssl directory not exist"
		exit -1
	fi

	cd ${current_root_dir}
	return
}

function install_openssl()
{
	LogWriter " * install openssl /usr/local/openssl/lib/* to /lib, /usr/local/lib, /usr/lib"

	cp /usr/local/openssl/lib/* /lib/ -rf
	cp /usr/local/openssl/lib/* /usr/local/lib/ -rf
	cp /usr/local/openssl/lib/* /usr/lib/ -rf

	if [ `uname -a | grep x86_64 -c ` -eq 1 ]; then
		{
			LogWriter " * install openssl /usr/local/openssl/lib/* to /lib64, /usr/local/lib64, /usr/lib64"
			cp /usr/local/openssl/lib/* /usr/lib64/ -rf
			cp /usr/local/openssl/lib/* /lib64/ -rf
			cp /usr/local/openssl/lib/* /usr/local/lib64/ -rf
		}
	fi

	cp -rf /usr/local/openssl/bin/openssl /bin/
}

openssh_backup()
{
	LogWriter " * Backup /usr/local/openssh to $back_dir/openssh_bak$dateString"
	cp -rf /usr/local/openssh $back_dir/openssh_bak$dateString
	return
}

openssh_build()
{
	LogWriter " * Build OpenSSH. prefix is ${openssh_prefix}"
	if [ -f ${openssh_dir}/${openssh_file}".tar.gz" ] ;then
		LogWriter "   > Uncompress ${openssh_dir}/${openssh_file}.tar.gz"
		tar -zxf ${openssh_dir}/${openssh_file}".tar.gz" -C $work_dir
	else
		LogWriter "   > [error] ${openssh_dir}/${openssh_file}.tar.gz not exist"
		exit -1
	fi

	cd ${work_dir}/${openssh_file}

	LogWriter "   > ./configure ${openssh_build_conf_option}"
	./configure ${openssh_build_conf_option}

	LogWriter "   > make"
	make -j4

	LogWriter "   > remove /usr/local/openssh"
	rm -rf /usr/local/openssh
	LogWriter "   > make install to /usr/local/openssh"
	make install

	LogWriter "   > sshd_config file copy from $back_dir/openssh_bak$dateString/etc/sshd_config"
	cp -rf $back_dir/openssh_bak$dateString/etc/sshd_config /usr/local/openssh/etc/sshd_config

	cd ${current_root_dir}
	return
}

openssh_binary_compress()
{
	cd /usr/local
	LogWriter " * compress OpenSSH dir(/usr/local/openssh) to ${openssh_dir}/openssh.tar.gz"
	if [ -d openssh ] ;then
		tar -zcf ${openssh_dir}"/"openssh.tar.gz openssh
	else
		LogWriter "   > [error] /usr/local/openssh directory not exist"
		exit -1
	fi

	cd ${current_root_dir}
	return
}

openssh_restart()
{
	service sshd restart
}

make_patch_file()
{
	cd ${current_root_dir}
	rm -rf sslssh_patch
	mkdir -p sslssh_patch
	mkdir -p sslssh_patch/${ssl_path}
	mkdir -p sslssh_patch/${ssh_path}
	cp -rf ${openssl_dir}/openssl.tar.gz sslssh_patch/${ssl_path}
	cp -rf ${openssh_dir}/openssh.tar.gz sslssh_patch/${ssh_path}
	cp -rf *.sh sslssh_patch

	tar cvzf sslssh_patch.tar.gz sslssh_patch
}

main()
{
	ParseArgument "$@"

	SetLogPath "$current_root_dir/logs"
	mkdir -p $logs_dir
	rm -rf $work_dir
	mkdir -p $work_dir
	mkdir -p $back_dir

	SetLogFileName "rebuild"
	LogWriter "----------OpenSSL, OpenSSH build script start----------"

	LogWriter "----------------OpenSSL build--------------------------"
	openssl_backup
	openssl_build
	openssl_binary_compress
	install_openssl


	LogWriter "----------------OpenSSH build--------------------------"
	openssh_backup
	openssh_build
	openssh_binary_compress

	openssh_restart

	make_patch_file

	LogWriter "------------------Build script end---------------------"
}

main "$@"
