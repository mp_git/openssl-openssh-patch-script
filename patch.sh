#!/bin/bash
# script version 2.0

currentPath=`pwd`
opensslPath="$currentPath/files/openssl"
opensslBuiltFilePath="$opensslPath/openssl.tar.gz"
opensshPath="$currentPath/files/openssh"
opensshBuiltFilePath="$opensshPath/openssh.tar.gz"
bashRpmFilePath="$currentPath/files/bash-3.2-33.el5_11.4.x86_64.rpm"
currentDate=`date +"%Y-%m-%d %H:%M"`
dateString=`date +"%Y%m%d_%H%M"`


function install_openssl()
{
	echo " * start install_openssl"
	echo "   >> backup openssl directory (/usr/local/openssl_bak$dateString)"
	mv /usr/local/openssl /usr/local/openssl_bak$dateString

	echo "   >> copy from built openssl file($opensslBuiltFilePath)"
	tar -zxf $opensslBuiltFilePath -C /usr/local > /dev/null 2> /dev/null

	echo "   >>> lib..."
	cp /usr/local/openssl/lib/* /lib/ -rf
	cp /usr/local/openssl/lib/* /usr/local/lib/ -rf
	cp /usr/local/openssl/lib/* /usr/lib/ -rf

	if [ `uname -a | grep x86_64 -c ` -eq 1 ]; then
		{
			echo "   >>> lib64..."
			cp /usr/local/openssl/lib/* /usr/lib64/ -rf
			cp /usr/local/openssl/lib/* /lib64/ -rf
			cp /usr/local/openssl/lib/* /usr/local/lib64/ -rf
		}
	fi

	echo "   >>> bin..."
	cp -rf /usr/local/openssl/bin/openssl /bin/

	echo "`date +"%Y-%m-%d %H:%M"`: openssl patch is completed."
	echo ""
}

function install_openssh()
{
	echo " * start install_openssh"
	echo "   >> backup openssh directory (/usr/local/openssh_bak$dateString)"
	mv /usr/local/openssh /usr/local/openssh_bak$dateString

	echo "   >> copy from built openssh file($opensshBuiltFilePath)"
	tar -zxf $opensshBuiltFilePath -C /usr/local > /dev/null 2> /dev/null
	cp -rf /usr/local/openssh_bak$dateString/etc/sshd_config /usr/local/openssh/etc/sshd_config
	cp -rf /usr/local/openssh_bak$dateString/etc/* /usr/local/openssh/etc

	echo "   >>> bin & sbin..."
	cp -rf /usr/local/openssh/bin/*  /usr/local/bin
	cp -rf /usr/local/openssh/sbin/* /usr/sbin

	echo "`date +"%Y-%m-%d %H:%M"`: openssh patch is completed."
	echo ""
}


function main()
{   
	/usr/local/apache2/bin/apachectl stop
	install_openssl
	install_openssh

	# service version check
	service sshd restart

	while true
	do
		processNum=`is httpd | grep -w -c "httpd"`
		if [ $processNum == 0 ]; then
			/usr/local/apache2/bin/apachectl start
			break
		fi
	done

	rpm -Uvh $bashRpmFilePath
	echo " * success bash update"
}

main "$@"
