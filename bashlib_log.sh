# Log Maker
# Author: JaeMoo Han
# Version: 0.1
# log format = [time|function_name] logstr

#!/bin/bash

dateform="%Y%m%d_%H%M%S"
str_now="`date +"$dateform"`"
logpath="./"
filename=`echo $0 | sed -e "s/^\.\///"`
logfilename="${filename}_${str_now}.log"

# LogWriter
# $1 = Log Message
LogWriter()
{
	SetTime
	logstr=${1}
	echo "[${str_now}|${FUNCNAME[1]}] ${logstr}" >> ${logpath}/${logfilename}
	echo "[${str_now}|${FUNCNAME[1]}] ${logstr}"
}

# SetLogPath
# $1 = log path
SetLogPath()
{
	logpath=${1}
}

SetLogFileName()
{
	filename=${1}
	logfilename="${filename}.log"
}

SetTime()
{
	str_now="`date +"$dateform"`"
}

