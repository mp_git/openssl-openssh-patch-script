# 사용법

## 패치 파일 만들기
### 1. 각각 openssl, openssh 소스 다운로드
rebuild.sh 스크립트 상단에 해당 버전 수정

repo에 포함된 버전 (openssl-1.0.2r, openssh-5.8p1)

openssl : https://www.openssl.org/source/

openssh : https://www.openssh.com/portable.html

### 2. rebuild 스크립트 실행
명령어 : nohup ./rebuild &
유의점 : ssh 접속 상태로 실행 불가

결과물 패치 파일 명 : sslssh_patch.tar.gz

### 3. 해당하는 PM 접속 및 패치
tar xvzf sslssh_patch.tar.gz

cd sslssh_patch

nohup ./patch.sh &

참고 : 일시적으로 sshd 서비스 끊김

